﻿using System;
using System.Collections;
using System.Collections.Generic;

// Define a Fruit class
public class Fruit
{
    public string Name { get; set; }

    public Fruit(string name)
    {
        Name = name;
    }
}

// Define an Iterator interface
public interface IIterator<T>
{
    bool HasNext();
    T Next();
}

// Define a concrete collection class
public class FruitCollection : IEnumerable<Fruit>
{
    private readonly List<Fruit> _fruits = new List<Fruit>();

    public void AddFruit(Fruit fruit)
    {
        _fruits.Add(fruit);
    }

    public IEnumerator<Fruit> GetEnumerator()
    {
        return new FruitIterator(_fruits);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    // Define the concrete iterator class
    private class FruitIterator : IIterator<Fruit>
    {
        private readonly List<Fruit> _fruits;
        private int _currentIndex = 0;

        public FruitIterator(List<Fruit> fruits)
        {
            _fruits = fruits;
        }

        public bool HasNext()
        {
            return _currentIndex < _fruits.Count;
        }

        public Fruit Next()
        {
            if (!HasNext())
                throw new InvalidOperationException("No more elements to iterate.");

            var fruit = _fruits[_currentIndex];
            _currentIndex++;
            return fruit;
        }
    }
}

public class Program
{
    public static void Main()
    {
        var fruitCollection = new FruitCollection();
        fruitCollection.AddFruit(new Fruit("Apple"));
        fruitCollection.AddFruit(new Fruit("Banana"));
        fruitCollection.AddFruit(new Fruit("Cherry"));

        // Iterate through the fruits using the Iterator
        var iterator = fruitCollection.GetEnumerator();
        while (iterator.MoveNext())
        {
            var fruit = iterator.Current;
            Console.WriteLine(fruit.Name);
        }
    }
}
